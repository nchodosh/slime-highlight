;;; slime-highlight.el --- Highlight defined symbols in a slime connected buffer
;;
;; Filename: slime-highlight.el
;; Description: Highlight defined symbols in a slime connected buffer
;; Author: Nate Chodosh
;; Maintainer: Nate Chodosh (concat "nchodosh" "@" "andrew." "cmu" ".com")
;; Copyright (C) 2013-2017, Nate Chodosh, all rights reserved.
;; Created: 30.04.2017
;; Version: 0
;; Package-Requires: ()
;; Compatibility: GNU Emacs: 22.x, 23.x, 24.x, 25.x
;;
;; Features that might be required by this library:
;;
;;   None
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;    Highlight defined or undefined symbols in Common Lisp. This package
;;   is based on the code in Highlight-Defined.
;;
;;  `shigh-highlight-mode' is a minor mode that highlights, in the
;;  current buffer, symbols that are known to be defined as Common
;;  Lisp functions or variables or both. It can also distinguish
;;  between local variables and ones from the package
;;  COMMON-LSIP. Alternatively, it can highlight symbols that are not
;;  known to be defined as functions or variables.
;;
;;  The current buffer should be slime connected.
;;
;;  Command `shigh-highlight-mode' toggles highlighting on/off.  The
;;  highlighting respects options `shigh-highlight-type' and
;;  `shigh-highlight-cl'.
;;
;;  Command `shigh-cycle' cycles highlighting among the available
;;  types and off, as follows: functions & variables > functions >
;;  variables > undefined > off .  It does this by changing the
;;  current value of option `shigh-highlight-type'.
;;
;;  You can of course customize the faces used for highlighting.  You
;;  might want, for instance, to have face `hdefd-functions' inherit
;;  from face `font-lock-function-name-face', and `hdefd-variables'
;;  inherit from `font-lock-variable-name-face'.  This is not the
;;  default because I don't find it so useful.
;;
;;
;;  Put this in your init file:
;;
;;    (require 'slime-highlight)
;;
;;  If you want to turn on this highlighting automatically whenever
;;  you enter Emacs-Lisp mode then you can do this in your init file: NO DON'T
;;
;;    (require 'slime-highlight)
;;    (add-hook 'lisp-mode-hook 'shigh-highlight-mode 'APPEND) DOES NOT WORK!!!
;;
;;  User option `shigh-highlight-type' controls what gets
;;  highlighted. Option `shigh-highlight-cl' controls if symbols from
;;  "COMMON-LISP" should be highlighted.
;; 
;;
;;  Faces defined here:
;;
;;    `shigh-functions', `shigh-variables', `shigh-clfunctions',
;;    `shigh-clvariables', `shigh-undefined'.
;;
;;  User options defined here:
;;
;;    `shigh-highlight-type', `shigh-highlight-cl'.
;;
;;  Commands defined here:
;;
;;    `shigh-highlight-mode'.
;;
;;  Non-interactive functions defined here:
;;
;;    `shigh-highlight'.
;;
;;  Internal variables defined here:
;;
;;    `shigh-face'.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Change Log:
;;
;; 2017/05/02 nchodosh
;;     Created from highlight-defined.el.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(eval-when-compile (require 'cl)) ;; case

(defgroup Slime-Highlight nil
  "Slime Powered Syntax Highlighting"
  :prefix "shigh-" :group 'matching :group 'font-lock :group 'programming)


(defface shigh-clfunctions
    '((t (:foreground "#00006DE06DE0"))) ; Like `font-lock-constant-face'.  Do not inherit.
  "Face used to highlight Lisp functions."
  :group 'Slime-Highlight :group 'faces)
(defface shigh-functions
    '((t (:foreground "#00006DE06DE0"))) ; Like `font-lock-constant-face'.  Do not inherit.
  "Face used to highlight Lisp functions."
  :group 'Slime-Highlight :group 'faces)

(defface shigh-clvariables
    '((t (:foreground "Orchid"))) ; Like `font-lock-builtin-face'.  Do not inherit.
  "Face used to highlight Lisp variables."
  :group 'Slime-Highlight :group 'faces)
(defface shigh-variables
    '((t (:foreground "Orchid"))) ; Like `font-lock-builtin-face'.  Do not inherit.
  "Face used to highlight Lisp variables."
  :group 'Slime-Highlight :group 'faces)

(defface shigh-undefined
    '((t (:foreground "Orange")))
  "Face used to highlight undefined Lisp symbols."
  :group 'Slime-Highlight :group 'faces)


(defcustom shigh-highlight-cl nil
  "Flag to determine if `shigh-highlight-mode' should highligh bultin symbols.
If the value is `t' then symbols from COMMON-LISP will be highlighted if it 
is `nil' then they won't be."
  :type 'boolean
  :group 'Slime-Highlight)

(defcustom shigh-highlight-type 'fns-and-vars
  "Type of highlighting to be done by `shigh-highlight-mode'.
If the value is `undefined', highlight symbols not known to be defined
as a function or a variable, using face `shigh-undefined'.
Otherwise, highlight defined symbols.

If highlighting defined symbols and a function and a variable have the
same name then:
* The name is highlighted.
* If the option value means that function names are highlighted then
  the name is highlighted with face `shigh-functions' (even if the
  occurrence is in fact used as a variable).
* If the value means that only variable names are highlighted then the
  name is highlighted with face `shigh-variables' (even if it is used
  as a function)."
  :type '(choice
          (const :tag "Functions and variables" fns-and-vars)
          (const :tag "Functions"               functions)
          (const :tag "Variables"               variables)
          (const :tag "Undefined symbols"       undefined))
  :group 'Slime-Highlight)

(defvar shigh-face nil
  "Symbol for face to use by `hdefd-highlight-mode'.")

(define-minor-mode shigh-highlight-mode
    "Toggle highlighting defined or undefined symbols in the buffer.
The current buffer should be in Lisp mode and connected to slime.
With prefix ARG, turn the mode on if ARG is positive, off otherwise.

Highlighting is governed by option `shigh-highlight-type': either
undefined symbols or defined symbols: functions or variables or both."
  :group 'Slime-Highlight
  (if shigh-highlight-mode
      (font-lock-add-keywords nil '((shigh-highlight . shigh-face)) 'APPEND)
    (font-lock-remove-keywords nil '((shigh-highlight . shigh-face))))
  (when font-lock-mode (font-lock-mode -1))
  (font-lock-mode 1)
  (when (if (> emacs-major-version 22)
            (called-interactively-p 'interactive)
          (called-interactively-p))
    (message "Slime Highlighting %s is now %s."
             (case shigh-highlight-type
               (fns-and-vars "FUNCTIONS and VARIABLES")
               (functions    "FUNCTIONS")
               (variables    "VARIABLES")
               (undefined    "UNDEFINED symbols"))
             (if shigh-highlight-mode "ON" "OFF"))))

(defun shigh-cycle ()
  "Cycle highlighting via `shigh-highlight-mode'.
Cycle among the possible values of option `shigh-highlight-type' and off."
  (interactive)
  (setq shigh-highlight-type  (if (not shigh-highlight-mode)
                                  'fns-and-vars
                                (case shigh-highlight-type
                                  (fns-and-vars  'functions)
                                  (functions     'variables)
                                  (variables     'undefined)
                                  (undefined     nil)
                                  ((nil)         'fns-and-vars))))
  (if shigh-highlight-type
      (unless shigh-highlight-mode (shigh-highlight-mode 1))
    (setq shigh-highlight-type  'fns-and-vars)
    (shigh-highlight-mode -1))
  (message "Slime Highlighting %s is now %s."
           (case shigh-highlight-type
             (fns-and-vars (if shigh-highlight-mode "FUNCTIONS and VARIABLES" "symbols"))
             (functions    "FUNCTIONS")
             (variables    "VARIABLES")
             (undefined    "UNDEFINED symbols"))
           (if shigh-highlight-mode "ON" "OFF"))
  (font-lock-fontify-buffer))


(defvar fboundp-str "(when (find-symbol \"%s\") (fboundp (find-symbol \"%s\")))")
(defvar boundp-str "(when (find-symbol \"%s\") (boundp (find-symbol \"%s\")))")
(defvar get-package-str "(when (find-symbol \"%s\") (symbol-package (find-symbol \"%s\")))")

(defun slime-fboundp (str)
  (read (downcase 
         (cadr (slime-eval `(swank:eval-and-grab-output 
                             ,(format fboundp-str str str)))))))
(defun slime-boundp (str)
  (read (downcase 
         (cadr (slime-eval `(swank:eval-and-grab-output 
                             ,(format boundp-str str str)))))))
(defun slime-get-package (str)
  (let ((str (cadr (slime-eval `(swank:eval-and-grab-output 
                                  ,(format get-package-str str str))))))
    (when (string-match "\"" str)
      (let* ((q1 (string-match "\"" str))
             (q2 (string-match "\"" str (1+ q1))))
        (substring str (1+ q1) q2)))))

(defun slime-local-symbolp (str)
  (not (string= (slime-get-package str) "COMMON-LISP")))

(defun trim-string (string)
  "Remove white spaces in beginning and ending of STRING.
White space here is any of: space, tab, emacs newline (line feed, ASCII 10)."
  (replace-regexp-in-string "\\`[ \t\n]*" "" (replace-regexp-in-string "[ \t\n]*\\'" "" string)))

(defun shigh-highlight (_limit)
  "Highlight Lisp functions and/or variables that are currently defined.
Use as a font-lock MATCHER function for `slime-highlight-mode'."
  ;; If your code uses any of these variable names too then bad - they will be highlighted:
  ;; `shigh-found', `shigh-highlight-type', `shigh-obj', and `shigh-opoint'.
  (let ((shigh-opoint  (point))
        (shigh-epoint nil)
        (shigh-found   nil)
        (move-error nil))
    (with-syntax-table lisp-mode-syntax-table 
      (skip-chars-forward "()#` []")
      (while (and (not shigh-found) (not (eobp)) (< (point) _limit))
        (condition-case () 
            (save-excursion
              (setq shigh-opoint  (point))
              (forward-sexp 1)
              (setq shigh-epoint (point)))
          (error (progn (setf move-error t) (setq shigh-epoint (point)))))
        (let ((shigh-str (trim-string
                          (upcase (buffer-substring-no-properties shigh-opoint 
                                                                  shigh-epoint)))))
          (if (and (not move-error) 
                   (> (length shigh-str) 0)
                   (symbolp (read shigh-str)))
            (let ((bndp (slime-boundp shigh-str))
                  (fbndp (slime-fboundp shigh-str))
                  (lclp (slime-local-symbolp shigh-str)))
             (if (and 
                  (not (member shigh-str '("NIL" "T")))
                                        ;(not (keywordp hdefd-obj))
                  (or (and (memq shigh-highlight-type '(fns-and-vars functions))
                           fbndp
                           (or (and (not shigh-highlight-cl) lclp)
                               shigh-highlight-cl))
                      (and (memq shigh-highlight-type '(fns-and-vars variables))
                           bndp
                           (or (and (not shigh-highlight-cl) lclp)
                               shigh-highlight-cl))
                      (and (eq shigh-highlight-type 'undefined)
                           (not fbndp)
                           (not bndp))))
                 (progn
                   (setq shigh-face
                         (if (and (memq shigh-highlight-type 
                                        '(fns-and-vars functions))
                                  fbndp)
                             (if lclp
                                 'shigh-functions
                               'shigh-clfunctions)
                           (if (eq shigh-highlight-type 'undefined)
                               'shigh-undefined
                             (if lclp
                                 'shigh-variables
                               'shigh-clvariables))))
                   (forward-sexp 1)
                   (setq shigh-found t))
               (progn
                 (if (looking-at "\\(\\sw\\|\\s_\\)")
                     (forward-sexp 1)
                   (forward-char 1))
                 (skip-chars-forward "()#`")
                 (setf move-error nil))))
            (progn
                 (if (looking-at "\\(\\sw\\|\\s_\\)")
                     (forward-sexp 1)
                   (forward-char 1))
                 (skip-chars-forward "()#`")
                 (setf move-error nil)))))
      (when shigh-found
       (set-match-data (list shigh-opoint shigh-epoint)))
      shigh-found)))

;;;;;;;;;;;;;;;;;;;;;;;

(provide 'slime-highlight)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; slime-highlight.el ends here
